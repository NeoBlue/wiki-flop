# Configuration de flop!EDT
La configuration de l'application se fait par l'intermédiaire d'un fichier de configuration au format INI décomposé en sections et en directives de configuration. Il peut (et doit) être édité via la commande `sudo nano /etc/flopedt/flopedt.ini`.

```
[flopedt]
secret_key=1j33g^v(3t3mc358yq0p#8bmv58^u4d_w3yz)e)9x0+n$v5ek(
tmp_directory=/var/flopedt/tmp
cache_directory=/var/flopedt/cache
static_directory=/var/flopedt/static
log_level=INFO
allowed_hosts=192.168.1.2,prod.flopedt.org

[database]
postgres_database=flop
postgres_hostname=localhost
postgres_username=flop
postgres_password=flop
postgres_port=5432

[redis]
redis_host=127.0.0.1
redis_port=6379

[gurobi]
license_file=/path/to/gurobi.lic

[email]
email_host="localhost"
email_port="587"
email_user="smtpuser"
email_password="smtppassword"
email_sender="no-reply@flop.edt"
email_use_ssl=True
```

**Section flopedt**

*Section pour la configuration générale de l'application.*

`secret_key`: Clé cryptographique utilisée pour les opérations cryptographiques de signature du moteur Django.

`tmp_directory`: Répertoire contenant les fichiers temporaires de l'application notamment les fichiers générés par les moteurs de résolution de contrainte. **Doit être inscriptible par l'utilisateur exécutant l'application**

`cache_directory`: Contient les fichiers cache générés par le moteur Django. **Doit être inscriptible par l'utilisateur exécutant l'application**

`static_directory`: Contient tout le contenu statique de l'application (Fichiers CSS, JS, HTML ...) de l'application.

`log_level`: Niveau de verbosité du système de journalisation. Les valeurs possibles sont:
- DEBUG
- INFO
- WARNING
- ERROR
- CRITICAL

`allowed_hosts` *(Depuis la version 0.5.1)*: Vous devez rajouter ici les adresses IP et les noms d'hôtes virtuels par lesquels l'application sera accessible. 

**Cette directive de configuration doit être cohérente avec ce qui est déclaré dans la configuration du serveur HTTP frontal faisant office de reverse proxy pour fonctionner correctement**

**Section database**

*Section pour la connexion à la base de données.*

`postgres_database`: Nom de la base de données hébergeant l'application.

`postgres_hostname`: Nom d'hôte / Adresse IP du serveur hébergeant la base de données.

`postgres_username`: Nom d'utilisateur pour la connexion au serveur de base de données.

`postgres_password`: Mot de passe pour la connexion au serveur de base de données.

`postgres_port`: Port pour la connexion au serveur de base de données

**Section redis**

*Le serveur REDIS est utilisé pour l'affichage des messages du moteur de résolution des contraintes. Il est indispensable pour le bon fonctionnement de l'application.*

`redis_host`: Nom d'hôte / Adresse IP pour la connexion au serveur REDIS.

`redis_port`: Port pour la connexion au serveur REDIS

**Section gurobi**

*Cette section est utile uniquement si le moteur GUROBI est utilisé.*

`license_file`: Chemin absolu vers le fichier de licence GUROBI

**Section email**

*Section de configuration du sous-sytème de messagerie utilisé pour les envois de mails.*

`email_host`: Nom d'hôte / Adresse IP du serveur de messagerie.

`email_port`: Port utilisé pour la connexion au serveur de messagerie.

`email_user`: Nom d'utilisateur pour la connexion au serveur de messagerie (A saisir si la connexion doit être authentifiée).

`email_password`: Mot de passe pour la connexion au serveur de messagerie (A saisir si la connexion doit être authentifiée).

`email_sender`: Adresse mail utilisée en tant qu'expéditeur des mails de l'application.

`email_use_ssl`: Activtation du chiffrement de la connexion au serveur de messagerie. *True* si la connexion est chiffrée - *False* si la connexion est en clair.