
# Philosophie
## Un logiciel libre
Opensource, gratuit, 
Développement coopératif, impliquant activement les étudiant⋅e⋅s
Dépendant d'autres outils libres (django, CBC, ...) auxquels on espère contribuer.

## Libérer du temps, créer du confort
Bégaudeau

## Auto-gestion et égalité
Tout le monde peut saisir ses préférences, modifier l'emploi du temps,
Rechercher l'égalité (la justice ?) dans le respect des préférences



# Principales fonctionnalités

## Afficher l'emploi du temps
### Filtres
### Export iCal
### Application Android / iOs


## Recueillir les préférences/contraintes 
### Préférences/contraintes pédagogiques
### Préférences/contraintes des usagères et usagers 


## Génération automatique de l'emploi du temps
### Comment ça marche ?
### Génération du meilleur emploi du temps
### Comprendre les raisons en cas d'infaisabilité


## Modifications possible pour tout le monde
