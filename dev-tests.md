# Tests des pré-analyses de contraintes

## Lancement des tests
Pour la configuration de l'environnement virtuel, 
voir [ici](installation-linux#configuration-de-l-environnement-virtuel-python).

À partir du répertoire racine du projet, on se place dans l'environnement virtuel puis dans le répertoire suivant :
```
source venv/bin/activate
cd FlOpEDT/
```

L'utilitaire de tests Django va se charger de chercher les fichiers nommés `test*.py` dans le répertoire courant si 
aucun path n'est précisé, dans le répertoire donné sinon et trouver tous les tests 
qu'il peut exécuter. 

Pour lancer tous les tests existants des fonctions `pre_analyse`, on tape :
```
./manage.py test TTapp/tests/test_pre_analyse
```

On peut aussi lancer des tests spécifiques à certains modules :
```
./manage.py test TTapp.tests.test_pre_analyse.test_core_constraints
```
ou à certaines classes :
```
./manage.py test TTapp.tests.test_pre_analyse.test_core_constraints.ConsiderTutorsUnavailabilityTestCase
```
ou à certains tests :
```console cd FlOpEDT/
./manage.py test TTapp.tests.test_pre_analyse.test_core_constraints.ConsiderTutorsUnavailabilityTestCase.test_consider_tutor_unavailability
```

**Important :** Lors des lancements de tests Django, l'utilitaire de test crée une nouvelle base de données réservée 
tout spécialement aux tests. Il n'utilise pas la base de données habituelle.
Il est possible qu'une erreur du type 
`Creating test database for alias 'default'...
Got an error creating the test database: permission denied to create database` surgisse. Il faut alors autoriser `flop_user` (ou le nom d'utilisateur que vous avez donné 
à [cette étape](installation-linux#mise-en-place-de-la-base-de-données)) à pouvoir créer une base de données :

On utilise le compte postgres :
```
sudo -iu postgres
```

Puis on lance l'interface psql :
```
psql
postgres=# ALTER USER flop_user CREATEDB; # On autorise flop_user à créer des bases de données
postgres=# \q # On quitte psql
```

## Écriture de nouveaux tests

Pour créer des nouveaux tests pour une méthode `pre_analyse` d'une contrainte, il y a deux phases : 
* la création des jeux de tests, où l'on définit des petits jeux de données, des cas triviaux où l'on sait si la
`pre_analyse`  devrait détecter une infaisabilité.
* l'écriture des classes et méthodes de test qui permettent de faire appel aux données définies dans les jeux de tests,
de lancer une méthode de pre_analyse dessus et de vérifier qu'on détecte bien une infaisabilité dans les cas où on 
aimerait qu'elle le soit mais aussi qu'on n'en détecte pas lorque l'on aimerait qu'aucune infaisabilité ne soit trouvée.

### Format des données renvoyées par `pre_analyse`
La méthode `pre_analyse` d'une classe héritant de `TTConstraint` renvoie une JsonResponse ? 
[TODO] un dictionnaire python indiquant si une infaisabilité liée à cette contrainte a pu être détectée. Ce dictionnaire rapporte aussi des phrases détaillant pourquoi un emploi du temps ne peut être généré si une infaisabilité est détectée. Le dictionnaire contient les données suivantes :
- `"status"` : type str, dont la valeur est "KO" si la pré-analyse permet de détecter une infaisabilité de l'emploi du temps liée à cette contrainte, "OK" sinon
- `"messages"` : type list de dict, la liste est vide si aucune infaisabilité est détectée, sinon les dictionnaires de la liste ont les clefs suivantes :
  - `"str"` : type str, dont le message détaille une raison d'infaisabilité détéctée
  - `"type"` : type str, dont la valeur représente le nom de la contrainte}
  -`"tutor"`, `"group"`, etc ... : type variable, dont la clé et la valeur dont une indication supplémentaire sur les données conflictuelles avec la contrainte
- `"period"` : [TODO] dict { "week": week.nb, "year": week.year }

### Création des jeux de test

Actuellement, les jeux de tests sont organisés par semaine. Sur chaque semaine sont posées des contraintes,
des préférences ou des indisponibilités différentes qui permettent de lancer un unique test unitaire.

### Saisie des données en base de données avec flop!EDITOR
Les données pour les tests comme les cours, les groupes ou les types de cours ont été saisies en base de données avec 
flop!EDITOR jusqu'à maintenant. 
Pour accéder au tutoriel de flop!EDITOR, il faut effectuer les étapes suivantes :

Saisir la commande suivante :
```
./manage.py runserver
```

Il faut ensuite accéder au site en tapant l'url suivante dans un navigateur :
```
http://localhost:8000
```

Cliquer sur l'onglet **flop!EDITOR**.

<img src="images/dev-tests/FlopEditorPanelHelp.png" alt="flopeditor panel" width="60%"/>

Cliquer sur **?** pour accéder à la documentation de **flop!EDITOR**.

<img src="images/dev-tests/DocPanelHelp.png" alt="Documentation flop!Editor panel" width="60%"/>

### Saisie des contraintes et associations entre données
Vous voudrez aussi créer des contraintes et indiquer quel **Tutor** doit donner un certain cours ou quel cours 
doit se faire avant un autre, etc ...

Pour cela, il faut un utilisateur Admin.

Pour créer un admin, il faut lancer un shell et ajouter les données d'un admin à la base de données :
```python
./manage.py shell_plus

t = Tutor(username='admin')
t.is_superuser = True
t.is_staff = True
t.set_password('password')
t.save()
t.departments.add(Department.objects.get(abbrev='default'))
```

Il est maintenant possible d'ajouter des contraintes en accédant à une interface. Il suffit de cliquer sur l'onglet **Admin**.
<img src="images/dev-tests/AdminPanelHelp.png" alt="Admin panel" width="60%"/>




#### Saisie des préferences utilisateur d'un **Tutor**
Il est difficile de saisir une par une les préférences d'un enseignant avec Admin.
Une solution est de se connecter en tant qu'enseignant (celui pour lequel vous voulez définir des préferences) et d'aller 
dans l'onglet **Preferences** du site. Il vous est possible de définir les préferences d'un enseignant avec une 
interface graphique pour une semaine de test.

<img src="images/dev-tests/PreferencesPanelHelp.png" alt="Preferences panel" width="60%"/>


**Remarque :** Il n'est pas possible de définir un jeu de test sur une semaine antérieure à celle du jour "d'aujourd'hui" avec cette méthode.

### Enregistrement des données de test sous le format Json
Un jeu de test est stocké sous le format `json`.
Une fois les données entrées en base, pour les récuperer sous le format json, il suffit de faire :
```
python3.7 manage.py dumpdata base people TTapp > data_test.json
```

#### Stockage des jeux de test Json
Pour l'instant, le fichier json est stocké dans le répertoire courant, c'est-à-dire `<path-to-project>/FlOpEDT`.
À terme, un répertoire dédié aux jeux de test pourrait être crée. **Attention :** penser à définir `FIXTURE_DIRS`. 

### Création des classes de test
La classe `ConstraintTestCase` hérite de la classe `TestCase` de Django. 
Elle propose en plus des méthodes d'assert classiques de `TestCase` des méthodes permettant de vérifier si le dictionnaire renvoyé par `pre_analyse`
d'une contrainte est la réponse attendue. 

La création d'une classe de test se fait ainsi :
```python
class ExempleTestCase(ConstraintTestCase):
  
    fixtures = [data_test.json] # On indique quel fichier de données mettre en base de données pour les tests
```

#### Redéfinition de la méthode `setUp()`
La méthode `setUp` permet d'initialiser les données dont on aura besoin pour les tests.
Par exemple :
```python
class ExempleTestCase(ConstraintTestCase):
  
    fixtures = [data_test.json]
    
    def setUp():
        ConstraintTestCase.setUp(self)
        self.constraint_type = str(<constraint_name>)
        
        self.week_13_2022 = Week.objects.get(year=2022, nb=13) # On récupère la semaine 13 de l'année 2022
        self.default_dep = Department.objects.get(abbrev="default") # On récupère le département default
        self.constraint_default_dep = ConsiderTutorsUnavailability.objects.get(department=self.default_dep) # On récupère la contrainte sur laquelle on effectue des tests
```

**Attention :** S'il y a plusieurs contraintes `ConsiderTutorsUnavailability` en base, la dernière ne fonctionnera pas, 
et il faudra privilégier un `filter` avec une boucle.

**Important :** Il faut penser à définir le type de la contrainte au tout début de la méthode `setUp` car `assertJsonResponseIsKO` l'utilise. On fait comme cela :
```python
def setUp():
    ConstraintTestCase.setUp(self)
    self.constraint_type = str(<constraint_name>) # Par exemple : "ConsiderTutorsUnavailability"
```

#### Écriture d'un test
L'organisation des tests peut se faire par méthode. Chaque méthode de la classe de test commençant par `test_` pourra 
tester une fonctionnalité de la pré-analyse en appelant la méthode `pre_analyse` sur une semaine precise.
Ceci se fait de la manière qui suit :
```python
class ConsiderTutorsUnavailabilityTestCase(ConstraintTestCase):
    # ...
    def test_consider_course_beginning_time(self):
        json_response_dict = self.constraint_default_dep.pre_analyse(week=self.week_2_2022)
        self.assertJsonResponseIsKO("3", json_response_dict)
```

#### Documentation des asserts
La documentation de ces deux méthodes 
- `assertJsonResponseIsOK(test_id, response_dict)` 
- `assertJsonResponseIsKO(test_id, response_dict)`

est trouvable dans `TTapp/tests/tools_test_pre_analyse/constraint_test_case.py`.