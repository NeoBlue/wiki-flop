1. Installation complète / déploiement (Linux)\
   ✧ [Installation via un package](/flopedt/FlOpEDT/-/wikis/Installation-via-un-package) \
   ✧ [Configuration web](/flopedt/FlOpEDT/-/wikis/Configuration-web)\
   ✧ [Paramètres du fichier de configuration](/flopedt/FlOpEDT/-/wikis/Configuration-de-flop)\
   ✧ [Dépannage](/flopedt/FlOpEDT/-/wikis/Section-depannage)
2. Import des données dans l'outil\
   ✧ [Données de base](import#import-des-donn%C3%A9es-de-base)\
   ✧ [Données de planification](import#import-des-donn%C3%A9es-de-planification)
3. Utilisation\
   ✧ [Par l'utilisateur·ice](utilisation)\
   ✧ [Par les gestionnaires](utilisation)
4. Documentation developpeurs\
   ✧ [Développement des pré-analyses](dev-pre-analyse)\
   ✧ [Améliorer la création des partitions par la prise en compte de nouvelles contraintes](dev-pre-analyse#partitions)\
   ✧ [Développement des tests de pré-analyse de contraintes](dev-tests)\
   ✧ [Rédiger une documentation](Rédiger-une-documentation)

5. Installation de test (old fashioned)\
   ✧ [A la main](/flopedt/FlOpEDT/-/wikis/Installation-Linux-(sans-Docker)) (Linux)\
   ✧ [En utilisant Docker](/flopedt/FlOpEDT/-/wikis/Installation-en-utilisant-Docker) (Linux ou Windows - ou Mac?)
---

<sup>(</sup>[<sup>Home</sup>](home)<sup> ; </sup>[<sup>Sidebar</sup>](\_sidebar)<sup>)</sup>
