On suit d'abord sur le serveur toutes les étapes d'installation qui
concernent [l'obtention de flop](installation#obtention-de-flop) et
[l'installation sans
docker](installation#installation-et-lancement-sans-docker) jusqu'au
lancement non inclus.

Reste à configurer le déploiement de l'application sur le serveur.

# Configuration du serveur web

## Installation

On installe nginx :
```
sudo apt install nginx
```

## Configuration du vhost

On crée le fichier vhost : 
```
sudo vi /etc/nginx/sites-available/fedt
```

Voici un exemple de vhost : 

```
server {
        listen 443 ssl;
        server_name l_adresse_de_votre_serveur;

        ssl_certificate /etc/ssl/fedt/fedt_iut-orsay_fr.pem;
        ssl_certificate_key /etc/ssl/fedt/fedt.iut-orsay.fr.key;
        ssl_session_timeout 5m;

        # Intermediate configuration. tweak to your needs.
        ssl_protocols TLSv1.2 TLSv1.3;# Requires nginx >= 1.13.0 else use TLSv1.2
        ssl_prefer_server_ciphers on;
        ssl_ciphers EECDH+AESGCM:EDH+AESGCM;
        ssl_session_cache shared:SSL:10m;
        ssl_ecdh_curve secp384r1; # Requires nginx >= 1.1.0
        ssl_dhparam /etc/ssl/fedt/dhparam2048.pem;
        add_header Strict-Transport-Security "max-age=63072000; includeSubdomains; preload";
        add_header X-Frame-Options DENY;
        add_header X-Content-Type-Options nosniff;
        ssl_session_tickets off; # Requires nginx >= 1.5.9
        ssl_stapling on; # Requires nginx >= 1.3.7
        ssl_stapling_verify on; # Requires nginx => 1.3.7
        add_header X-XSS-Protection "1; mode=block";
        resolver ip.de.votre.dns1 ip.de.votre.dns2 valid=300s;
        resolver_timeout 5s;

        # serve static files
        location /static/ {
          alias /var/www/html/edt/static/;
        }

        # pass requests for dynamic content to gunicorn
        location / {
          proxy_pass http://127.0.0.1:8000;
          proxy_redirect     off;
          proxy_set_header   Host $host;
          proxy_set_header   X-Real-IP $remote_addr;
          proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
          proxy_set_header   X-Forwarded-Host $server_name;
        }

        location /solver/ {
          proxy_pass http://127.0.0.1:8000;
          proxy_http_version 1.1;
          proxy_set_header Upgrade $http_upgrade;
          proxy_set_header Connection "upgrade";

          proxy_set_header   Host $host;
          proxy_redirect     off;
          proxy_set_header   X-Real-IP $remote_addr;
          proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
          proxy_set_header   X-Forwarded-Host $server_name;

        }
}
```

## Activation du vhost

On active le vhost : 
```
cd /etc/nginx/
sudo ln -s /etc/nginx/sites-available/fedt /etc/nginx/sites-enabled/fedt
```

On redémarre nginx :
```
sudo systemctl restart nginx
```

## Création du répertoire de fichiers statiques

En production, Django a besoin d'un répertoire où il va stocker (et
d'où il va servir) tous les fichiers statiques (e.g. les fichiers
`javascript`).

On crée le répertoire statique pour flop : 
```
sudo mkdir /var/www/html/edt/
```

On donne les droits à l'utilisateur et au groupe `fedt` :
```
sudo chown -R fedt:fedt /var/www/html/edt/
```


# Configuration du service sous systemd

## Service systemd sans Gurobi

On crée le service `/etc/systemd/system/fedt.service` : 

```
[Unit]
Description=FlopEdt
After=network.target

[Service]
PIDFile=/run/daphne/pid
User=fedt
Group=fedt
WorkingDirectory=/home/fedt/FlOpEDT/FlOpEDT
ExecStart=/home/fedt/FlOpEDT/venv/bin/daphne -b 127.0.0.1 -p 8000 --verbosity 3 --access-log=/tmp/flop.log FlOpEDT.asgi:application
ExecReload=/bin/kill -s HUP $MAINPID
ExecStop=/bin/kill -s TERM $MAINPID
Restart=on-abort
Environment="DJANGO_LOG_LEVEL=DEBUG"

[Install]
WantedBy=multi-user.target
```

On active le service : 
```
sudo systemctl enable fedt.service
```

## Service systemd avec la prise en compte de Gurobi

On crée le service `/etc/systemd/system/fedt.service` : 

```
[Unit]
Description=FlopEdt
After=network.target

[Service]
PIDFile=/run/daphne/pid
User=fedt
Group=fedt
WorkingDirectory=/home/fedt/FlOpEDT/FlOpEDT
Environment="GUROBI_HOME=/home/fedt/gurobi902/linux64"
Environment="PATH=YOUR_PATH:/home/fedt/gurobi902/linux64/bin"
Environment="LD_LIBRARY_PATH=/home/fedt/gurobi902/linux64/lib"
Environment="GRB_LICENSE_FILE=/home/fedt/gurobi.lic"
ExecStartPre=/bin/echo ${GUROBI_HOME} ${PATH}
ExecStart=/home/fedt/FlOpEDT/venv/bin/daphne -b 127.0.0.1 -p 8000 --verbosity 3 --access-log=/tmp/flop.log FlOpEDT.asgi:application
ExecReload=/bin/kill -s HUP $MAINPID
ExecStop=/bin/kill -s TERM $MAINPID
Restart=on-abort
Environment="DJANGO_LOG_LEVEL=DEBUG"

[Install]
WantedBy=multi-user.target
```

On active le service : 
```
sudo systemctl enable fedt.service
```


# Configuration de Django

En utilisant éventuellement le compte Unix dédié, on va modifier la
configuration du fichier `manage.py` :
```
 sudo -i -u fedt
 vi FlOpEDT/FlOpEDT/manage.py
```
On remplace la ligne : 
```
 os.environ.setdefault("DJANGO_SETTINGS_MODULE", "FlOpEDT.settings.local")
```
par 
```
 os.environ.setdefault("DJANGO_SETTINGS_MODULE", "FlOpEDT.settings.dockerless-production")
```

**Note :** le fichier `local.py` n'est plus pris en compte par Django,
les deux fichiers lus sont désormais `base.py` et `dockerless-production.py`.

On édite ensuite le fichier `dockerless-production.py` : 
```
vi /home/fedt/FlOpEDT/FlOpEDT/FlOpEDT/settings/dockerless-production.py
```

```
from .base import *

STATIC_ROOT = '/var/www/html/edt/static/'

SECRET_KEY = 'ssssssssssssssssssssssssssssssssss'

DEBUG = False

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'flop_db_yyyyyyyyyyyyyyyyy',
        'USER': 'flop_user',
        'PASSWORD': 'xxxxxxxxxxxxxxxxxxxxx',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
    }
}
```

On ajoute également les hôtes autorisés à se connecter au serveur en
ajoutant la ligne suivante dans le `dockerless-production.py` :

```
ALLOWED_HOSTS = ['localhost','l_adresse_de_votre_serveur']
```

## Copie des fichiers statiques

On se reconnecte éventuellement avec l'utilisateur `fedt` : 
```
 sudo -i -u fedt
```
On utilise le virtualenv : 
```
cd FlOpEDT/
source venv/bin/activate
```

On génère les fichiers statiques qui seront distribués par Nginx : 
```
python3.7 manage.py collectstatic
214 static files copied to '/var/www/html/edt/static'.
```

# Lancer FlOpEDT

On démarre le serveur web :
```
 sudo systemctl start nginx.service
```

Et on démarre le service `systemd` :
```
sudo systemctl start fedt.service
```
