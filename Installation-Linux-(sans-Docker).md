# Principales dépendances

- Python 3.11
- [Django 4](https://www.djangoproject.com/) pour le site
- [PostgreSQL](https://www.postgresql.org/) pour la base de données
- [PuLP](https://github.com/coin-or/pulp) pour la modélisation en ILP (Integer Linear Programming)
- Un solveur de ILP, e.g. [CBC](https://projects.coin-or.org/Cbc), [Gurobi](gurobi.com)
- [Redis](https://redis.io) pour le cache de Django (optionnel)

# Obtention de flop

## Installation préalables

### Vi

Pour visualiser et éditer des fichiers

```
sudo apt install vi
```

### Git

```
sudo apt install git
```

### Éventuellement Python

#### Ubuntu (>20.04)

Python3.11 n'est plus dans les dépôts par défaut. Ajoutez le dépôt suivant :

```
sudo apt update
sudo apt install software-properties-common
sudo add-apt-repository ppa:deadsnakes/ppa
```

#### Toute version

`sudo apt install python3.11 python3.11-minimal python3.11-venv python3-pip python3-virtualenv virtualenv`

## whoami

**Important :** dans la suite, on supposera que vous êtes l'utilisateurice `fedt` ; si ce n'est pas le cas, remplacer toutes les occurrences de `fedt` par votre nom d'utilisateurice. Si vous voulez créer l'utilisateurice dédié·e `fedt`, voir [ici](#utilisateurice-d%C3%A9di%C3%A9e).

## Récupération des sources du logiciel

On se place dans le répertoire de l'utilisateurice `fedt` (rappel : on considère que `fedt` est votre nom d'utilisateurice, pour connaître le votre, tapez `whoami` (et utilisez ce nom à la place de `fedt` dans la suite)) :

```
cd /home/fedt
```

On récupère le dépôt git :

```
git clone https://framagit.org/flopedt/FlOpEDT.git
```

On se déplace dans le répertoire :

```
cd FlOpEDT/
```

On change de branche pour passer sur celle de développement :

```
git checkout dev
```

# Installation et lancement sans docker

## Configuration de l'environnement virtuel python

On utilise la commande suivante :

```
virtualenv -p /usr/bin/python3.11 venv
```

On utilise ce virtualenv :

```
source venv/bin/activate
```

On met à jour PIP :

```
pip3 install --upgrade pip
  Requirement already up-to-date: pip in ./venv/lib/python3.11/site-packages (20.0.2)
```

On installe les modules python3 nécessaires :

```
pip3 install -r requirements.txt
```

On installe également :

```
pip3 install python-memcached
pip3 install ipython
pip3 install daphne
```

On sort de l'environnement virtuel :

```
deactivate
```

## Mise en place de la base de données

On installe le système de gestion de bases de données `postgreSQL` :

```
sudo apt install postgresql
```

On utilise le compte postgres :

```
sudo -i -u postgres
```

On va utiliser `psql` pour créer la base de données :

```
psql                                            # On lance la commande psql
postgres=# CREATE USER flop_user WITH PASSWORD 'your_password'; # On crée l'utilisateur flop_user
postgres=# CREATE DATABASE flop_database_public_dev WITH OWNER flop_user;  # On crée la base de données
postgres=# \q                              # On quitte l'invite de commande psql
```

NB:

- les valeurs `flop_user`, `your_password` et `flop_database_public_dev` sont celles définies dans le fichier de configuration de Django `/home/fedt/FlOpEDT/FlOpEDT/FlOpEDT/settings/local.py`. Vous pouvez donc modifier ces valeurs dans les commandes potgresql ET dans le fichier.
- Si vous êtes en train de déployer le site sur un serveur, le fichier de settings à considérer n'est pas `local.py` mais `dockerless-production.py`. Il s'agit alors de prendre les valeurs correspondantes, ou de les y modifier.

**Rappel :** si votre nom d'utilisateurice Linux n'est pas `fedt` mais par exemple `tartif`, le fichier de settings se trouve alors à `/home/tartif/FlOpEDT/FlOpEDT/FlOpEDT/settings/local.py`.

On sort du compte postgres :

```
exit
```

## Installation d'un solveur de programme linéaire en nombres entiers

Pour générer des emplois du temps qui satisfont les contraintes, il est nécessaire d'installer un solveur de programme linéaire en nombres entiers. Il en existe plusieurs, libres ou propriétaires, et il suffit d'en installer un.

### CBC -- Common Public License

On installe le paquet suivant :

```
sudo apt install coinor-cbc
```

### Gurobi -- Propriétaire

Attention : Gurobi n'est pas gratuit, il nécessite une licence d'utilisation.

#### Création d'un compte sur le site de l'éditeur

Pour obtenir une licence académique, il faut créer un compte sur [le site de l'éditeur](https://www.gurobi.com/downloads/end-user-license-agreement-academic/). Il faut ensuite télécharger l'archive compressé du logiciel gurobi [sur le site de l'éditeur](https://www.gurobi.com/downloads/gurobi-optimizer-eula/). Une fois le lien récupéré, on peut télécharger le logiciel sur le serveur :

```
cd /usr/local/src
wget "https://packages.gurobi.com/9.0/gurobi9.0.2_linux64.tar.gz"
```

NB: on installe ici la version 9.0.2, mais n'hésitez pas à installer la version la plus récente.

#### Installation de Gurobi

En tant que `root`, on décompresse l'archive :

```
tar xzf gurobi9.0.2_linux64.tar.gz
```

On déplace le répertoire dans `/home/fedt` pour éviter des problèmes de droits, mais le logiciel pourrait très bien être installé de manière globale.

```9da552dc-8085-11ea-ae9e-020d093b5256
mv gurobi902/ /home/fedt/
```

#### Activation de la licence

On récupère la commande à taper [sur le site de l'éditeur](https://www.gurobi.com/downloads/licenses/). On se déplace ensuite dans le répertoire où sont stockés les fichiers exécutables :

```
cd gurobi902/linux64/bin
```

On exécute la commande pour activer la licence :

```
./grbgetkey votre_numero_de_licence_obtenu_depuis_le_site_de_gurobi
```

La commande devrait vous demander ou stocker la licence, si vous ne modifiez rien, vous devriez obtenir ceci :

```
info  : grbgetkey version 9.0.2, build v9.0.2rc0
info  : Contacting Gurobi key server...
info  : Key for license ID 439793 was successfully retrieved
info  : License expires at the end of the day on 2020-06-16
info  : Saving license key...

In which directory would you like to store the Gurobi license key file?
[hit Enter to store it in /home/fedt]: 

info  : License 439793 written to file /home/fedt/gurobi.lic
```

**Important :** Les licences académiques ne durent que 3 mois, et sont renouvelables, donc à renouveler.

#### Installation du module python pour Gurobi

Il faut au préalable s'assurer d'avoir deux fichiers :

- `.bashrc`
- `.profile`

Il faut ensuite éditer le fichier `.bashrc` et ajouter :

```
# Gurobi 
export GUROBI_HOME="/home/fedt/gurobi902/linux64"
export PATH="${PATH}:${GUROBI_HOME}/bin"
export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${GUROBI_HOME}/lib"
```

On peut forcer le chargement du fichier .bashrc (pour éviter de se reconnecter) :

```
source .bashrc
```

On peut désormais installer le module python pour gurobi :

```
cd FlOpEDT/                # On se déplace dans le répertoire FlOpEDT pour charger notre environnement virtuel python
source venv/bin/activate   # On charge l'environnement virtuel python
cd                         # On revient à la racine de notre 'home'
cd gurobi902/linux64/      # On se déplace dans le répertoire ou est stocké le fichier d'installation du module python
python3.11 setup.py install # On lance l'installation
```

Vous devriez obtenir ceci :

```
 running install
 running build
 running build_py
 creating build
 creating build/lib
 creating build/lib/gurobipy
 copying lib/python3.11_utf32/gurobipy/__init__.py -> build/lib/gurobipy
 copying lib/python3.11_utf32/gurobipy/gurobipy.so -> build/lib/gurobipy
 running install_lib
 creating /home/fedt/FlOpEDT/venv/lib/python3.11/site-packages/gurobipy
 copying build/lib/gurobipy/gurobipy.so -> /home/fedt/FlOpEDT/venv/lib/python3.11/site-packages/gurobipy
 copying build/lib/gurobipy/__init__.py -> /home/fedt/FlOpEDT/venv/lib/python3.11/site-packages/gurobipy
 byte-compiling /home/fedt/FlOpEDT/venv/lib/python3.11/site-packages/gurobipy/__init__.py to __init__.cpython-311.pyc
 running install_egg_info
 Writing /home/fedt/FlOpEDT/venv/lib/python3.11/site-packages/gurobipy-9.0.2.egg-info
 removing /home/fedt/gurobi902/linux64/build
```

Si tout est bon, en lançant python, vous devriez pouvoir importer le module sans erreur :

```
 python3.11
 Python 3.11.1 (default, Dec 20 2019, 18:57:59) 
 [GCC 8.3.0] on linux
 Type "help", "copyright", "credits" or "license" for more information.
 >>> import gurobipy
 >>> exit()
 deactivate
```

## Configuration de Django

On retourne dans l'environnement virtuel :

```
cd FlOpEDT/                  # On se déplace dans le bon répertoire
source venv/bin/activate     # On active notre environnement virtuel
cd FlOpEDT/                  # On se déplace à nouveau dans le bon répertoire ( ~/FlOpEDT/FlOpEDT$ )
FLOP_CONFIG_FILE=../config_file/flopedt.ini python3.11 manage.py migrate  # On lance les migrations
```

Ceci nous donne quelque chose de ressemblant à :

```
(venv) fedt@yaddle:~/FlOpEDT/FlOpEDT$ FLOP_CONFIG_FILE=../config_file/flopedt.ini python3.11 manage.py migrate
Operations to perform:
  Apply all migrations: TTapp, admin, auth, base, configuration, contenttypes, displayweb, easter_egg, people, quote, sessions, solve_board
Running migrations:
  Applying base.0001_initial... OK
  Applying contenttypes.0001_initial... OK
  Applying contenttypes.0002_remove_content_type_name... OK
  Applying auth.0001_initial... OK
  Applying auth.0002_alter_permission_name_max_length... OK
  Applying auth.0003_alter_user_email_max_length... OK
  Applying auth.0004_alter_user_username_opts... OK
  Applying auth.0005_alter_user_last_login_null... OK
  Applying auth.0006_require_contenttypes_0002... OK
  Applying auth.0007_alter_validators_add_error_messages... OK
  Applying auth.0008_alter_user_username_max_length... OK
  Applying auth.0009_alter_user_last_name_max_length... OK
  Applying people.0001_initial... OK
  Applying base.0002_auto_20180712_0749... OK
  Applying base.0003_auto_20180722_1209... OK
  Applying base.0004_auto_20181030_1440... OK
  Applying base.0005_auto_20181022_0910... OK
  Applying people.0002_tutor_departments... OK
  Applying people.0003_auto_20181129_1019... OK
  Applying people.0004_auto_20181219_0858... OK
  Applying people.0005_auto_20190116_2204... OK
  Applying base.0006_edtversion_department... OK
  Applying base.0007_auto_20181025_0914... OK
  Applying base.0008_regen_department... OK
  Applying base.0009_merge_20181031_0831... OK
  Applying base.0010_auto_20181101_0724... OK
  Applying base.0011_auto_20181102_1101... OK
  Applying base.0012_period_department... OK
  Applying base.0013_coursetype_department... OK
  Applying base.0014_auto_20181113_1042... OK
  Applying base.0015_auto_20181219_0858... OK
  Applying base.0016_grouptype_department... OK
  Applying base.0017_auto_20190120_1719... OK
  Applying people.0005_grouppreferences_studentpreferences... OK
  Applying people.0006_auto_20190310_1746... OK
  Applying people.0007_auto_20190311_1920... OK
  Applying base.0015_auto_20181116_1522... OK
  Applying base.0016_scheduledcourse_day... OK
  Applying base.0017_auto_20181116_1554... OK
  Applying base.0018_auto_20181117_2138... OK
  Applying base.0019_auto_20181117_2154... OK
  Applying base.0020_scheduledcourse_tday... OK
  Applying base.0021_remove_scheduledcourse_day... OK
  Applying base.0022_auto_20181117_2200... OK
  Applying base.0023_timegeneralsettings... OK
  Applying base.0024_timegeneralsettings_days... OK
  Applying base.0025_auto_20181118_1420... OK
  Applying base.0026_courseslot_generalslot_step... OK
  Applying base.0027_auto_20181126_1441... OK
  Applying base.0028_auto_20181126_1614... OK
  Applying base.0029_auto_20181201_1708... OK
  Applying base.0030_auto_20181201_1713... OK
  Applying base.0031_auto_20190117_1008... OK
  Applying displayweb.0001_initial... OK
  Applying displayweb.0002_auto_20190424_1055... OK
  Applying base.0032_auto_20190424_1054... OK
  Applying TTapp.0001_initial... OK
  Applying TTapp.0002_auto_20180712_0749... OK
  Applying TTapp.0003_auto_20180712_0749... OK
  Applying TTapp.0004_limitedroomchoices... OK
  Applying TTapp.0005_auto_20181102_1101... OK
  Applying TTapp.0006_auto_20181108_1045... OK
  Applying TTapp.0007_auto_20181113_1031... OK
  Applying TTapp.0008_auto_20181219_0858... OK
  Applying TTapp.0009_auto_20190116_1031... OK
  Applying TTapp.0010_customconstraint... OK
  Applying TTapp.0011_auto_20190527_2050... OK
  Applying TTapp.0012_auto_20190603_2046... OK
  Applying TTapp.0013_auto_20191028_2047... OK
  Applying admin.0001_initial... OK
  Applying admin.0002_logentry_remove_auto_add... OK
  Applying admin.0003_logentry_add_action_flag_choices... OK
  Applying people.0008_auto_20190611_1444... OK
  Applying base.0033_timegeneralsettings_default_preference_duration... OK
  Applying people.0009_userdepartmentsettings... OK
  Applying people.0010_auto_20190618_1243... OK
  Applying people.0011_auto_20190624_0757... OK
  Applying people.0012_tutor_max_hours_per_day... OK
  Applying people.0013_auto_20190926_1259... OK
  Applying people.0014_remove_fullstaff_department... OK
  Applying people.0015_userdepartmentsettings_is_admin... OK
  Applying people.0016_auto_20200302_1718... OK
  Applying base.0034_room_departments... OK
  Applying base.0035_auto_20190627_1131... OK
  Applying base.0036_Refactor_code_to_english... OK
  Applying base.0037_auto_20191106_1656... OK
  Applying base.0038_auto_20191108_1448... OK
  Applying base.0039_auto_20191122_1532... OK
  Applying base.0040_auto_20191127_1543... OK
  Applying base.0041_module_url... OK
  Applying base.0042_auto_20200302_1718... OK
  Applying base.0043_auto_20200302_1749... OK
  Applying base.0044_auto_20200302_1752... OK
  Applying base.0045_auto_20200303_2329... OK
  Applying base.0046_roomsort_tutor... OK
  Applying base.0047_module_description... OK
  Applying base.0047_auto_20200310_1118... OK
  Applying base.0048_auto_20200309_1826...Subrooms
Departments
RoomPreferences: OK
 OK
  Applying base.0049_auto_20200310_1233... OK
  Applying base.0050_auto_20200310_1424... OK
  Applying base.0051_remove_room_basic... OK
  Applying configuration.0001_initial... OK
  Applying configuration.0002_delete_updateconfig... OK
  Applying displayweb.0003_tutordisplay... OK
  Applying easter_egg.0001_initial... OK
  Applying easter_egg.0002_auto_20200313_1653... OK
  Applying people.0017_notificationspreferences... OK
  Applying quote.0001_initial... OK
  Applying sessions.0001_initial... OK
  Applying solve_board.0001_initial... OK
```

## Initialisation des données

### Import d'un jeu de données test

Pour tester l'outil, on peut importer les données fournies qui servent de jeu de test :

```
FLOP_CONFIG_FILE=../config_file/flopedt.ini python3.11 manage.py loaddata ../dump.json
```

Ce qui nous donne :

```
 Installed 10863 object(s) from 1 fixture(s)
```

**Important :** Si on souhaite supprimer ces données, on peut utiliser la commande :

```
FLOP_CONFIG_FILE=../config_file/flopedt.ini python3.11 manage.py flush
```

### Création d'un super utilisateur

On peut créer un superutilisateur (qui aura accès à toutes les fonctionnalités de l'application):

```
FLOP_CONFIG_FILE=../config_file/flopedt.ini python3.11 manage.py createsuperuser
```

puis se laisser guider...

## Lancement de Django

Pour lancer le serveur manuellement, on utilise :

```
FLOP_CONFIG_FILE=../config_file/flopedt.ini python3.11 manage.py runserver
```

Vous pouvez désormais normalement accéder au site en tapant dans un navigateur :

```
http://localhost:8000
```

## Utilisateurice dédié·e

Il est possible que vous vouliez créer un·e utilisateurice dédié·e `fedt`. Vous devrez être connecté sous `fedt` (par exemple via `sudo -i -u fedt`) pour la majeure partie des étapes, mais attention, sans action supplémentaire `fedt` ne sera pas `sudoer`, et il faudra donc changer d'utilisateurice pour exécuter les commandes en `sudo`.

### Création de l'utilisateurice

```
sudo addgroup fedt  # On crée le groupe
```

puis on crée l'utilisateurice avec les options :

```
* -c : commentaire
* -d : indique le home de l'utilisateur
* -g : indique le groupe de l'utilisateur
* -r : crée un compte système
* -m : crée le répertoire home de l'utilisateur
* -s : défini le shell de l'utilisateur
```

```
sudo useradd -c "Utilisateur de FlOpEDT" -d /home/fedt -s /bin/bash -m -g fedt -r fedt
```

### Gurobi

Par ailleurs, pour Gurobi, il faudra modifier les droits sur le répertoire :

```
chown -R fedt:fedt /home/fedt/gurobi902
```

Suivant comment vous avez créé l'utilisateur `fedt`, les fichiers `.bashrc` et `.profile` peuvent être absents, vous pouvez les copier avec les commandes :

```
sudo -i -u fedt
cp /etc/skel/.bashrc .
cp /etc/skel/.profile .
```