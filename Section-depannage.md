## Le navigateur affiche le message Bad Request (400)

Assurez vous que la directive `allowed_hosts` de la section `flopedt`du fichier de configuration `flopedt.ini` contient le nom d'hôte virtuel déclaré dans la configuration du serveur reverse proxy.

*flopedt.ini*
```
[flopedt]
allowed_hosts=prod.flopedt.org
```

*nginx*
```
server {
    server_name prod.flopedt.org
}
```

Si aucune section `server_name`, n'est configurée et que l'application doit être accédee par l'adresse IP, il faut déclarer celle-ci au niveau de la directive `allowed_hosts`.

*flopedt.ini*
```
[flopedt]
allowed_hosts=11.22.33.44
```

## Le navigateur affiche le message Server Error (500)

Il vous est possible d'accéder au log du serveur django de l'application pour identifier l'erreur.
```
journalctl -fu flopedt
```

## Accès au shell django
Pour accéder au shell django (qui permet de lancer les fonctions et méthodes de flop et/ou de communiquer avec la BD postgresql via l'ORM de django) faites la commande:
```
sudo flop_admin shell_plus
```

NB: de manière générale, `flop_admin` vous permet d'accéder aux différentes méthodes du contrôleur django `manage.py`...