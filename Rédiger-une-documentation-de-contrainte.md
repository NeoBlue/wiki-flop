# **Documentation**

## **Informations**
Le serveur python utilisant un système de cache, il est important de redémarrer celui-ci pour que les modifications sur les documentations soient effectives.

## **Rédaction**
### **Format**
La documentation doit simplement être rédigée en Markdown. Le support des balises HTML est donc inclus.
### **Images**
L'intégration d'image se fait de la même manière que dans tout document Markdown.
Il y a alors 2 types d'images : 
- Les images externes avec une url vers un site.
- Les images locales, avec une url de la forme `../images/dossier/image.png`
    - Le chemin local doit absolument commencer par `../images/`
    - Le format recommandé pour les images est le `.png`

Il est préférable que chaque image dans le serveur ait un nom unique.

### **Interpolation**

L'interpolation permet d'afficher la valeurs des paramètres associée à la contrainte.
Dans le cas où le paramètre référence l'ensemble des instances de ce dernier, le rendu affichera "All".

À l'heure actuelle, ces interpolations s'appliquent uniquement lorsque la documentation est affichée depuis la popup d'une contrainte existante sauvegardée en base.

#### **Syntaxe**
Pour faire une demande d'interpolation, il faut utiliser la syntaxe suivante : `{{ <Nom du paramètre> }}` où le nom de paramètre est exactement celui de ce paramètre dans la classe python associée à la contrainte. 

#### **Rendu**

Soit la documentation suivante servant d'exemple:
```Markdown
### **Description**
Garantit que les groupes {{ groups }} ont une pause repas (ou autre) d'au moins {{ lunch_length }} minutes
entre {{ start_lunch_time }} et {{ end_lunch_time }} les {{ weekdays }}.
### **Exemples**
| Contexte | OK | Not Ok |
|:--:|:--:|:--:|
| groups : All <br> start_time : 12h30 <br> end_time : 14h  <br> lunch_length : 60 minutes | ![Situation autorisée](../images/ok_tutors_lunch_break.png) | ![Situation interdite](../images/forbidden_tutors_lunch_break.png)|

### **Paramètres** :
- start_time: minutes depuis minuit (entier)
- end_time: minutes depuis minuit (entier)
- lunch_length : minutes (entier)
```

Pour la documentation d'exemple on obtiens le rendu suivant :.
![](uploads/212a234094f4fcf98aeb57bcd9f7ce2b/img1.png)

#### **Cas de l'écran de modification/création**

Dans le cas d'un affichage depuis l'écran de création ou de modification, les paramètres de la contrainte ne sont pas instanciés. L'interpolation n'affichera pas les valeurs des paramètres mais simplement le nom du paramètre à interpoler. Pour la documentation d'exemple on obtiens le rendu suivant : [image 2]
![image2](uploads/d9914060f45716f85e89ddce10c9d790/image2.png)

#### **Interpolation invalide**

L'afficheur de documentation traite les interpolations uniquement si le paramètre demandé est présent dans la contrainte côté client. Si ce paramètre n'est pas présent alors un warning sera affiché dans la console :

![image3](uploads/cc5d89c3ca61a4bb94281f7b03e5fcf5/image3.png)

Dans ce cas, l'interpolation ne sera pas rendu :
![image4](uploads/36bd1216e516f2f6726a68fb530dd484/image4.png)

## **Règles**
Au démarrage du serveur le système gérant les documentations filtre ces dernières selon plusieurs règles qui, si elles ne sont pas respectées, empêche les documentations invalides d'être servies :
- Une image avec un lien local est non trouvée lors du démarrage du serveur.
- Une balise `<component>` est détectée.