# Développement des pré-analyses

## Méthode `pre_analyse` d'une TTConstraint
Pour créer une nouvelle pré-analyse concernant une contrainte, il faut créer une méthode `pre_analyse(self,week)` dans 
la classe correspondant à la contrainte.

Le paramètre `week` donne la semaine sur laquelle on veut réaliser la pré-analyse.
Si `week` vaut `None`, les données considérées sont des données appliquées à toutes les semaines, leur paramètre `weeks` est aussi `None`.

## Partitions
Le système de pré-analyse se base sur l'utilisation de partition (`base/partition.py`). Ces partitions représentent
des intervalles de temps. Un système permettant de créer des partitions pour considérer d'autres contraintes dans la 
pré-analyse d'une contrainte existe dans `base/partition_with_constraints.py`.

### Available et forbidden
À chaque intervalle correspondent deux informations booléennes (toujours initialisées à `False`) :

- **"available"** qui est mis à `True` lorsqu'un des "possible_tutors" est disponible
- **"forbidden"** qui est mis à `True` dès lors qu'une contrainte interdit le cours sur cet intervalle
  (ex : Un prof qui ne fait pas cours le lundi, un groupe qui ne peut avoir de cours le jeudi ...)

La mise en place de ces deux variables permet de gérer, dans la prise en compte des
préférences des utilisateurs, la distinction entre un professeur qui doit être 
nécessairement présent et un professeur qui fait partie d'un ensemble dans 
lequel au moins un des professeurs doit être disponible.

L'attribut `supp_tutor` d'un cours correspond à l'ensemble des profs qui doivent tous être nécéssairement présents et 
l'attribut `tutor` correspond à un prof qui doit être présent. Si ce dernier est laissé vide lors de la création d'un cours, 
au moins un des profs qui enseignent dans le module doit etre présent. On regardera alors les
préférences des différents professeurs pouvant donner un cours dont le module est celui
du cours.

### Création de partitions
Pour créer une partition, il existe trois fonctions dans `base/partition_with_constraints.py`.

- `create_group_partition_from_constraints` qui renverra une partition dans laquelle 
auront été prises en compte les contraintes actives parmi **NoGroupCourseOnDay** et **GroupsLunchBreak**.
- `create_tutor_partition_from_constraints` qui renverra une partition dans laquelle 
auront été prises en compte les contraintes actives parmi **NoTutorCourseOnDay**, **TutorsLunchBreak** et **ConsiderTutorsUnavailability**.
- `create_course_partition_from_constraints` qui renverra une partition dans laquelle 
auront été prises en compte les contraintes actives parmi les contraintes concernant le groupe qui a ce cours et
concernant un ensemble de professeurs possibles (réduit à un unique professeur si le cours a un tutor attribué)
et/ou un ensemble de professeurs nécessaires (correspondant à l'attribut `supp_tutor`).

### Remarque concernant `complete_tutor_partition` dans ConsiderTutorsUnavailability
**ATTENTION :**

Lors d'une pré-analyse, quand on souhaite vérifier les valeurs de **"available"** et **"forbidden"**,
il est important de ne pas oublier que les **"available"** sont modifiés uniquement si une contrainte
**ConsiderTutorsUnavailability** est active sur la semaine sur laquelle la pré-analyse est effectuée.

Si vous voulez écrire une pré-analyse qui prend en compte les "user preferences".
Et donc que vous voulez regardez les **"available"** pour vérifier les disponibilités.
N'oubliez surtout pas de traiter le cas où la contrainte **ConsiderTutorsUnavailability** n'est pas active.
Dans le cas contraire, votre pré-analyse renverra toujours un **"status"** à KO si la contrainte n'est pas activée.
(Car vous regarderez les **"available"** mais ceux-ci seront toujours à `False`).

Dans tous les cas, il faut toujours impérativement vérifier que **"forbidden"** n'a pas été mis à `True`.

## Prise en compte systématique de certaines TTConstraints
Il est possible de rajouter des informations concernant des **TTConstraints** dans une partition lors de la création de 
cette dernière. 

Typiquement, des contraintes impactant directement des journées ou des créneaux interdits peuvent être intégrés à une partition.
On peut penser à des données comme des vacances, des jours reservés pour l'alternance ou des pauses déjeuner personnalisées.
Les contraintes que l'on veut systématiquement prendre en compte à la création d'une partition pour un groupe, un cours 
ou un professeur donné implémentent les méthodes `complete_tutor_partition` ou `complete_group_partition`.

### Contraintes concernées
Contraintes prises en compte à la création d'une partition pour un **Tutor** :

```
TTConstraints
   ├── core_constraints.py
   │   ├── ConsiderTutorsUnvailability
   │   
   ├── no_course_constraints.py
   │   ├── NoTutorCourseOnDay
   │   
   ├── orsay_constraints.py
   │   ├── TutorLunchBreak
```
Contraintes prises en compte à la création d'une partition pour un **StructuralGroup** :

```
TTConstraints
   ├── no_course_constraints.py
   │   ├── NoGroupCourseOnDay
   │   
   ├── orsay_constraints.py
   │   ├── GroupLunchBreak
```
### Ajout de complete_tutor/group_partition

Pour prendre en compte de nouvelles contraintes lors de la création d'une partition,
il suffit d'implémenter une méthode `complete_group/tutor_partition` dans la classe de cette contrainte.

Attention : Cette méthode doit être implémentée judicieusement en appelant la méthode `add_slot` de la classe **Partition**
de manière à modifier les paramètres **"available"** ou **"forbidden"** comme désiré.

Pour commencer à implémenter un `complete_tutor/group_partition`, il faut vérifier si les données des attributs de la 
classe de contrainte qui nous intéresse concernent les données passées en paramètres.
Ainsi, pour la méthode `complete_tutor_partition(self, partition, tutor, week)` d'une TTConstraint concernant un **Tutor**,
on regardera si self.tutors contient le `tutor` passé en paramètres :
```python
if self.tutors.filter(username=tutor.username):
    # Completer la partition avec partition.add_slot()
```

De même, il est important de vérifier que la contrainte est active sur la semaine `week` passée en paramètres, 
puisque c'est la semaine sur laquelle on veut réaliser un pré-analyse.
```python
if self.weeks.filter(Q(year=week.year) & Q(nb=week.nb)):
    # Completer la partition avec partition.add_slot()
```
