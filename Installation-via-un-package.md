# 1. Introduction
Des paquetages d'installation sont disponibles pour les distributions:
 * Debian 11 - Bullseye
 * Ubuntu 20.04 - Focal Fossa
 * Ubuntu 22.04 - Jammy Jellyfish

 Des dépôts de paquetages sont disponibles pour toutes ces distributions à l'adresse https://packages.flopedt.org

 La suite de cette documentation décrit comment configurer l'accès à ces dépots puis l'installation en elle même.

 # 2. Configuration des dépôts
 ## 2.1 Import de la clé publique
 Afin de garantir l'intégrité de l'installation, les métadonnées des dépots d'installation sont signées. Il est donc nécessaire d'installer localement la clé publique afin de garantir leur intégrité lors du téléchargement.

 La clé est disponible à l'adresse https://packages.flopedt.org/gpg/flopedt.key. Pour l'importer, exécuter la commande suivante en tant sur `root`:


 ``` bash
sudo curl https://packages.flopedt.org/gpg/flopedt.asc | sudo gpg --dearmor -o /etc/apt/trusted.gpg.d/flopedt.gpg 
 ```
## 2.2 Déclaration du dépôt

**Pour Debian 11 - Bullseye:**

 ``` bash
echo "deb https://packages.flopedt.org/bullseye bullseye main" | sudo tee /etc/apt/sources.list.d/flopedt.list > /dev/null
 ```
**Pour Ubuntu 20.04 - Focal Fossa:**

 ``` bash
echo "deb https://packages.flopedt.org/focal focal main" | sudo tee /etc/apt/sources.list.d/flopedt.list > /dev/null
 ```
**Pour Ubuntu 22.04 - Jammy Jellyfish:**

 ``` bash
echo "deb https://packages.flopedt.org/jammy jammy main" | sudo tee /etc/apt/sources.list.d/flopedt.list > /dev/null
 ```
 # 3. Installation

Mettre à jour les sources d'installation
 
``` bash
sudo apt update
```

Installer l'application

 ``` bash
sudo apt install flopedt
```

# 4. Configuration web
Passer à l'[étape suivante](configuration-web)...