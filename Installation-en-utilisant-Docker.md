# 1. Pré-requis

## Linux
Mettre à jour les paquets apt avec la commande `sudo apt update` 

Installer `make` avec la commande `sudo apt install make`

Installer Git avec la commande `sudo apt install git`

Installer Docker Engine en suivant [cette documentation](https://docs.docker.com/engine/install/).

## Windows

### Installer WSL2.

Pour cela :

- Ouvrir une fenêtre PowerShell (ou invite de commande)
- Taper `wsl --install`
- Lancer l'application `Ubuntu` depuis le menu Démarrer pour ouvrir le terminal Linux. Ce terminal sera celui dans lequel taper les commandes de la partie 2 ci dessous. Saisir un login et un mot de passe à ne pas oublier :wink: 
- Taper dans ce terminal `sudo apt install update && sudo apt install make && sudo apt install git` en utilisant le mot de passe défini à l'étape précédente. 

Pour plus de détails, voir [cette documentation](https://learn.microsoft.com/fr-fr/windows/wsl/install).

### Installer Docker Desktop 

Pour cela, [cliquer ici](https://desktop.docker.com/win/main/amd64/Docker%20Desktop%20Installer.exe) ou suivre [cette documentation](https://docs.docker.com/desktop/install/windows-install/). 

### Démarrer Docker Desktop

Dans le menu Démarrer... et passer à l'étape 2.

## Avec Mac 

On n'a pas encore testé....... mais ça devrait être faisable :smile: 

# 2. Lancement de l'application sous docker

Cloner le dépôt de flopedt : `git clone https://framagit.org/flopedt/flopedt.git flopedt`.

Rentrer dans le répertoire ainsi créé : `cd flopedt`

Passer sur la branche `dev` : `git checkout dev`.

Lancer la commande `make config`.

Lancer ensuite la commande `make build`.

Lancer enfin la commande `make start` (ou `make start_verbose` si vous voulez un log plus fourni...). 

L'application sera accessible à l'adresse [http://localhost](http://localhost) .

Après ce processus d'initialisation, les commandes utiles sont `make start` et `make stop`.

## Problèmes éventuels 
- la commande `make` utilise la variable d'environnement `CONFIG`. Si celle-ci est utilisée par ailleurs, il faudra spécifier `CONFIG=development make ...`.
- Si vous obtenez une erreur de type `permission denied`, il est possible que votre utilisateur UNIX n'ait pas les droits relatifs à l'utilisation de docker. L'ajouter au groupe `docker` devrait régler le problème :
```
sudo groupadd docker 
sudo usermod -aG docker votre_utilisateur
```

## Import de données de tests
Vous pouvez importer les données d'exemple contenues dans le fichier [dump.json.bz2](./dump.json.bz2) (qui est une base pour jouer avec l'interface) avec la commande : `make init`.

Vous pourrez alors vous connecter avec l'utilisateur `MOI` et le mot de passe `passe`. Cet utilisateur possède les droits associés aux responsables des emplois du temps. Pour la vision d'une personne enseignante classique, utiliser l'un des autres login (En fait, tous les utilisateurs ont le même mot de passe `passe` !).

## Lancement en mode production

Deux exemples de configuration sont disponibles pour exécuter l'application avec Docker : `development` et `production`. La configuration `development` est utilisée par défaut par les cibles du fichier Makefile. Pour utiliser la configuration `production`, les commandes deviennent :

- `make config`
- `CONFIG=production make install`

puis

- `CONFIG=production make [build|init|start|stop]`