# Bien définir ses données pour ne pas faire flop!

Pour pouvoir utiliser le solveur comme générateur d'emploi du temps, il faut avoir clairement défini les données utiles. Cette définition initiale se fait par le remplissage et l'import de tableur excels, en deux temps.

La première étape consiste à définir les ressources qui ne devraient pas (trop) être amenées à évoluer au cours de l'année scolaire : 
- les enseignants, 
- les salles, regroupées par types 
- les groupes d'étudiants, et leurs sous-groupes éventuels
- les modules (UE en fac, ou matière en lycée...)
- la structure de l'emploi du temps (durée des créneaux, horaires, ...)

La deuxième étape consiste à construire une planification de l'année scolaire, au travers de la définition de tous les *cours à placer* pour chaque semaine.
On appelle *cours à placer* un objet caractérisé par:
- une semaine de l'année
- un type (TD, cours magistral, examen, ...)
- un module (maths, anglais, ...)
- un (ou plusieurs) enseignant⋅e⋅s
- un (ou plusieurs) groupes d'étudiant⋅e⋅s
- un type de salle


# Import des données de base

## Récupérer le tableur de définition de la base
- Se connecter avec un super_utilisateur (Si vous n'en avez pas créé, [voir ici](https://framagit.org/flopedt/FlOpEDT/-/wikis/installation-linux#cr%C3%A9ation-dun-super-utilisateur)).
- Cliquer sur l'onglet `Importer`, puis dans la partie Etape 1 - Option A cliquer sur le bouton `Télécharger`, et enregistrer le fichier.

<img src="images/download_database_file.gif" alt="télécharger le fichier de base" width="60%"/>

## Remplir le fichier avec vos données
Remplir un fichier *pour chaque département* de votre établissement. On appelle département une entité autonome dans sa gestion de l'emploi du temps, mais qui peut partager des ressources (salles, enseignants) avec les autres départements de l'établissement. Il peut s'agir d'une formation (L3 Anglais, ou d'une UFR, ou ...)
Vous pouvez bien sûr choisir de n'avoir qu'un seul département.

Noter qu'il est impératif, pour chacun des onglets du fichier, de respecter les indications en rouge situées en haut du tableur. 

<img src="images/alertes_tableur.png" alt="alertes à respecter" width="100%"/>

Voici maintenant comment remplir chaque onglet

### Onglet Paramètres
- Définir l'heure de début et de fin de journée, ainsi que l'heure éventuelle de pause méridienne.

  NB: si votre établissement n'a pas de pause, définir une heure de début et de fin de pause identique.
- Définir la granularité, en minutes, à laquelle les gens vont pouvoir donner leurs (in)disponibilités.
- Définir par un X les jours ouvrables de votre établissement
- Définir enfin les périodes d'enseignement, caractérisées par un identifiant, une semaine de début et une semaine de fin. Par exemple :

<img src="images/périodes.png" alt="Périodes" width="30%"/>

### Onglet `Intervenants`
- Définir un identifiant court (si possible 4 caractères maximum)
- L'adresse doit être une adresse email valide
- La colonne `Employeur` est facultative, et n'est utile qu'en cas de Statut "Vacataire"

### Onglet `Salles`
- Les groupes sont des ensembles de salles qui sont affectées *EN MÊME TEMPS* à un cours.
- Les catégories sont un ensemble de salles ou de groupes de salles dont l'un sera affecté à un cours donné. Dans la mesure ou les catégories formeront une des caractéristique des cours, il s'agit de les penser soigneusement.

  Exemple: J'ai trois salles d'informatique, deux petites (B01 et B02) et une grande (A12).
  - Certains TP peuvent être dans une quelconque de ces salles d'informatique, je crée la catégorie *TPInfo* et j'y inclue les salles en question.
  - Certains cours peuvent utiliser les deux petites en même temps, ou la grande. Je crée le groupe B01-02 (et je lui affecte les deux salles). Je crée la catégorie *CoursInfo* et je lui affecte les deux possibles.
  - Un examen d'informatique ne pourra être que dans la salle A12, je crée une catégorie ExamInfo en y affectant cette seule salle

<img src="images/salles.png" alt="salles" width="100%"/>

### Onglet `Groupes`
- Définir les promotions de votre département. Les promotions correspondent à une classe d'âge, susceptible d'avoir des cours communs ou des contraintes communes.
  
  Exemple: DUT1, DUT2, LP. Ou 2nde, 1ère, Terminale.

- Définir des natures de groupes pertinentes. Par exemple: TP ou TD. Ou bien: normal, langues.
  
  NB: Si cette étape n'est pas pertinente pour vous, créer une nature unique.

- Définir les groupes d'étudiant⋅e.s qui forment l'architecture de votre formation. Pour ces groupes, dits *structuraux*, chaque groupe doit appartenir à une promotion, et peut avoir un sur-groupe.
  
  NB: Notre outil impose une structure d'arbre pour les groupes structuraux, donc:
  - il convient que chaque promotion ait un groupe unique qui soit sans surgroupe (qui peut d'ailleurs avoir le même identifiant que la promotion)
  - chaque autre groupe structural doit avoir un unique sur-groupe.
  
  Voici un exemple de structure de groupes, et le tableur correspondant:

<img src="images/groupes_image.png" alt="salles" width="40%"/> <img src="images/groupes_tableur.png" alt="salles" width="55%"/>

- définir les groupes *transversaux*. Il s'agit de groupes qui ne forment pas l'architecture, mais la traversent (ex: groupes d'option, de langue, etc.)
Exemple: dans mon architecture ci dessus, les étudiant⋅e.s des groupes 1 et 2 peuvent choisir une option parmi anglais et italien, celles et ceux du groupe 4 peuvent choisir entre anglais ou espagnol, et ceux du groupe 3 ont le choix entre les 3 langues. Pour chaque langue, les cours sont communs aux étudiant⋅e⋅s des différents groupes qui ont choisi cette option. Lorsqu'un cours de langue a lieu, un cours d'une autre langue peut avoir lieu en même temps mais aucun autre cours ne le peut. 
On aura alors le schéma suivant :
<img src="images/groupes_transversaux.png" alt="Groupes transversaux" width="100%"/>

### Onglet `Modules`

- Définir la liste des modules, en leur attribuant un identifiant unique, une abréviation courte (4 caractères environ, pour l'affichage web), un nom plus explicite, une promotion, un enseignant responsable,  Un *Module* est l'équivalent d'une UE dans le système universitaire, ou d'une matière au lycée. 

### Onglet `Cours`
- Définir les types de cours, leur durée, les types de groupes qui sont concernés par ces types de cours, et les heures auxquels ces cours peuvent débuter.

  NB : cette liste peut être longue si votre granularité est à la demie-heure... Ne pas hésiter à rajouter des colonnes !

## Déployer la base à l'aide du tableur rempli
- Se connecter avec un super_utilisateur (Si vous n'en avez pas créé, [voir ici](https://framagit.org/flopedt/FlOpEDT/-/wikis/installation-linux#cr%C3%A9ation-dun-super-utilisateur)).
- Cliquer sur l'onglet `Importer`, puis dans l'encadré `Département` de la partie `Etape 1 - Option A` saisir le nom du département et son abbréviation (courte), et enfin sélectionner le tableur rempli et l'importer.

  <img src="images/upload.gif" alt="charger le fichier de base" width="60%"/>

  NB: 
  - Vous aurez très probablement des erreurs à corriger... Corrigez-les!
  - il faut faire cette opération pour *chaque* département, en prenant soin de mettre des identifiants identiques pour les ressources partagées (enseignants et salles)

# Import des données de planification

## Récupérer le fichier de planification vierge
- Se connecter avec un super_utilisateur (Si vous n'en avez pas créé, [voir ici](https://framagit.org/flopedt/FlOpEDT/-/wikis/installation-linux#cr%C3%A9ation-dun-super-utilisateur)).
- Cliquer sur l'onglet `Importer`, puis dans la partie `Etape 2` cliquer sur le bouton `Télécharger` après avoir choisi le département voulu. 
- Enregistrer le fichier.
<img src="images/download_planif_file.gif" alt="télécharger le fichier de planification" width="60%"/>

## Saisir vos données de planifications

### Commençons par observer le fichier...

Le fichier de planification contient trois onglets que nous verrons ensuite : Rules, ModuleTutorsAssignation et Récap.

Il contient également un onglet pour chacune des périodes de votre département, telles que [définies ici](import#onglet-paramètres).
Observons l'un quelconque de ces onglets. 

On y trouve
- Une entête:
   - Dans les premières colonnes on retrouve les caractéristiques d'un *cours à placer* (telles que définies [ici](import#bien-définir-ses-données-pour-ne-pas-faire-flop)) : module, type de cours, durée, prof, type de salle, groupes.
   - Les colonnes suivantes correspondent aux semaines de la période concernées.
   - deux lignes labellisées `Max` et `Total` (qui serviront à baliser le volume de cours)
- Un pied de page avec des éléments de récapitulatif utiles.

NB : il est impératif de *NE PAS MODIFIER* la structure des colonnes, ni cette entête et ni ce pied de page.

On trouve également:
- un bloc pour chaque module de la période (Cf [ici](import#onglet-modules)), et au sein de ce bloc :
  - une ligne verte foncée pour chaque nature de groupes (Cf [ici](import#onglet-groupes))
  - une ou plusieurs lignes vert claires pour chaque groupe ayant cette nature (Cf [ici](import#onglet-groupes))

C'est dans ces blocs que nous allons saisir les cours à planifier.

### Principe de la saisie des cours à planifier
Pour chaque semaine, il s'agit de saisir *tous* les cours qui sont à planifier, et notant le nombre de cours ayant les mêmes caractéristiques présents dans la semaine.

Par exemple, sur cette image :

<img src="images/exemple_de_planification.png" alt="Exemple de planification" width="100%"/>

En semaine 37 le groupe 1G2 a 2 TD de COMM1 avec le prof GEST dans une salle de type E007. Le même groupe en a un seul avec les mêmes caractéristiques en semaine 39.

Noter que:
- il est parfaitement possible de rajouter des lignes au sein des blocs (si par exemple un même groupe a un même cours avec des profs différents, ou dans des salles de types différents)
- les lignes importantes sont les lignes vert clair. Les lignes vert foncé ne servent qu'à du récap.
- les règles qui régissent le remplissage des premières colonnes se trouvent dans l'onglet `Rules`
- dans le cas où vous souhaitez ne générer qu'un emploi du temps type (ou quelques uns: semaine A, semaine B) amené à se répéter, ne remplir qu'une (ou quelques) colonne. Une fois généré, l'emploi du temps sera étendu aux semaines concernées.

### Vérifications et récapitulatif
Ce document va permettre de vérifier que les cours planifiés satisfont quelques contraintes de base.

- Si tous les groupes ont les mêmes cours au sein de la promotion, la ligne 3, `TOTAL`, et la dernière colonne `VERIF` vont vous être utiles. 
  En effet, la ligne 3, `TOTAL` compte la somme des *heures* correspondant aux nombres insérés dans les lignes vert foncé. Ce nombre est donc le nombre d'heures de cours que les étudiants auront dans la semaine *A CONDITION QUE* ces cours se répercutent de manière identique pour tous les groupes. Dans ce cas, en insérant dans la ligne `MAX` le nombre max d'heures que vous souhaitez qu'ielles fassent, vous aurez un premier check utile.
  Toujours dans ce cas, la colonne VERIF va afficher `OK` si la somme des cours attribués aux différents groupes correspond bien au nombre attendus d'après la ligne vert foncé.
- En insérant l'identifiant d'un⋅e enseignant⋅e en case `1B` de m'onglet `Récap`, vous aurez accès au nombre total d'heures que cette personne fait semaine par semaine et période par période, ainsi que globalement.
- Vous pouvez ajouter des informations sur les cours en insérant une ou plusieurs des remarques proposées dans l'onglet `Rules`:
  - D si les cours (en nombre pair) doivent être d'affilée
  - ND si les cours (en nombre pair) doivent être des jours différents
  - Atype si les cours doivent être après celui du type *type*
  - ...

## Déployer le fichier de planification
- Se connecter avec un super_utilisateur (Si vous n'en avez pas créé, [voir ici](https://framagit.org/flopedt/FlOpEDT/-/wikis/installation-linux#cr%C3%A9ation-dun-super-utilisateur)).
- Cliquer sur l'onglet `Importer`, puis dans la partie `Etape 2 - Fichier de planification des cours complété` choisir le nom du département et sélectionner le tableur rempli et l'importer.

  <img src="images/upload_planif.gif" alt="charger le fichier de planif" width="60%"/>

  NB: 
  - Vous aurez très probablement des erreurs à corriger... Corrigez-les et ré-importez le fichier!
  - Le bouton `Stabilisation` n'est utile que dans le cas d'une planification avec peu de semaines types (Semaine A / Semaine B), lorsque vous souhaitez une stabilité entre ces deux semaines. Plus précisément, cela imposera que les cours identiques d'une semaine sur l'autre soient placés au même endroit.