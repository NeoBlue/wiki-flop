# Création de la base de données

flop!EDT est qualifié pour être utilisé uniquement avec le moteur de base de données PostgreSQL. L'application nécessite d'avoir une base de données dédiée protégée par un mécanisme d'authentification par couple nom d'utilisateur / mot de passe.

La procédure ci-après décrit comment se connecter au serveur PostgreSQL.

- **Accès au serveur de base de données**
```bash
sudo -i -u postgres
```
- **Création de la base de données et de l'utilisateur associé**

```bash
# Connexion au serveur
psql
```
puis             
``` sql
CREATE USER <nom_utilisateur> WITH PASSWORD '<mot_de_passe>';
CREATE DATABASE <nom_base_de_données> WITH OWNER <nom_utilisateur>;
\q                  
```
# Installation de la licence GUROBI
**Licence Fixe**

Si vous disposez d'une licence fixe pour GUROBI, celle-ci doit être générée pour l'utilisateur système `flopedt` créé automatiquement lors de l'installation du paquetage.

Le paquetage installe automatiquement une version de l'outil `grbgetkey` permettant de générer le fichier de licence à partir d'une clé obtenue sur le site de GUROBI.

Pour générer le fichier de licence, suivre la procédure suivante:
- **Obtenir un shell en tant que l'utilisateur `flopedt`**
``` bash
sudo -i su - flopedt --shell=/bin/bash
```

- **Générer le fichier de licence**
``` bash
grbgetkey 718c633b-b9cd-4d01-b2b7-XXXXXXXXXXXX
info  : grbgetkey version 10.0.1, build v10.0.1rc0
info  : Contacting Gurobi license server...
info  : License file for license ID 2372379 was successfully retrieved
info  : License expires at the end of the day on 2024-04-27
info  : Saving license file...

In which directory would you like to store the Gurobi license file?
[hit Enter to store it in /opt/venvs/flopedt]: /tmp

info  : License 2372379 written to file /tmp/gurobi.lic/gurobi.lic
info  : You may have saved the license key to a non-default location
info  : You need to set the environment variable GRB_LICENSE_FILE before you can use this license key
info  : GRB_LICENSE_FILE=/tmp/gurobi.lic
```

Il est conseillé de déplacer le fichier de licence dans le répertoire de configuration `/etc/flopedt` et de lui limiter ses droits d'accès :

``` bash
sudo mv /tmp/gurobi.lic /etc/flopedt/
sudo chown root:flopedt /etc/flopedt/gurobi.lic
sudo chmod 0640 /etc/flopedt/gurobi.lic
```

**Licence Flottante**

La licence flottante n'est pas liée à un utilisateur ni à une machine. Il suffit donc de télécharger le fichier et de le déposer dans le répertoire /etc/flopedt/ et d'y appliquer les bons droits.

``` bash
sudo chown root:flopedt /etc/flopedt/gurobi.lic
sudo chmod 0640 /etc/flopedt/gurobi.lic
```

# Configuration de l'application

Le paquetage d'installation déploie le fichier de configuration système `/etc/flopedt/flopedt.ini`. Tous les paramètres du fichier de configuration sont décrits [ici](Configuration-de-flop.md).

La configuration doit être éditée en tant que `root`.

```bash
sudo vim /etc/flopedt/flopedt.ini
```

Afin de disposer d'une configuration initiale fonctionnelle, il est nécessaire de configurer les paramètres suivants : 

- **secret_key (Section flopedt)**

Avant de démarrer l'application, il est nécessaire de configurer une clé secrète **forte** et **unique**. Pour générer un token, il est possible d'utiliser le code python suivant:

```python
python -c "import secrets; print(secrets.token_urlsafe())"
```
- **section database**

Tous les paramètres de cette section doivent être configurés pour permettre la connexion à la base de données.

- **license_path (section gurobi)**

Le paramètre `license_path` doit contenir le chemin absolu vers le fichier de licence GUROBI. Si ce chemin est incorrect, GUROBI ne sera pas disponible dans la liste des solvers.

*Rappel: Si la licence GUROBI est de type fixe, elle doit être générer pour l'utilisateur `flopedt`.

- **section email**

Tous les paramètres de cette section doivent être configurés pour permettre l'envoi des notifications par mail.

# Configuration du serveur NGINX

Afin que l'application fonctionne correctement, il est nécessaire de mettre en place et activer un reverse proxy. Ci-après, les blocs de configuration à rajouter dans votre fichier de configuration NGINX: 

- **Déclaration du backend daphne**

``` bash
upstream daphne-backend {
 server 127.0.0.1:8000;
}
```

- **Configuration reverse proxy**
``` bash
server {
    
    ...

    # Service du contenu statique de l'application
    location /static/ {
        root /var/flopedt/;
    }

    # Transfert des requêtes au serveur DAPHNE
    location / {
        proxy_pass http://daphne-backend; 
        proxy_http_version 1.1;
        proxy_redirect off;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For  $proxy_add_x_forwarded_for;
        }

    # Websocket pour l'affichage des messages du moteur de résolution de contraintes
    location /solver/ {
        proxy_pass http://daphne-backend;         proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";

        proxy_set_header   Host $host;
        proxy_redirect     off;
        proxy_set_header   X-Real-IP $remote_addr;
        proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
    }
}

```
**Important**: La directive `server_name` du bloc `server` doit être configurée avec un nom d'hôte virtuel déclaré dans la directive de configuration `allowed_hosts` du fichier de configuration `flopedt.ini`.

# Démarrage des services

- **Démarrage du serveur PostgreSQL**

``` bash
systemctl start postgresql
```
- **Démarrage du serveur NGINX**

``` bash
systemctl start nginx
```
- **Démarrage du serveur REDIS**
``` bash
systemctl start redis
```

- **Démarrage de l'application**

```  bash
systemctl start flopedt
```
# Démarrage automatique des services

```bash
systemctl enable postgresql
systemctl enable nginx
systemctl enable redis
systemctl enable flopedt
```

# Opérations de première installation

- **Insertion du schéma de la base de données et des données initiales**

La première étape consiste à alimenter la base de données avec son schéma et les premières données nécessaires.

```
flop_admin migrate
```

## Création de l'utilisateur administrateur

Afin de pouvoir se connecter au panneau d'administration de l'application et/ou à l'onglet d'[import de données](import.md), il est nécessaire qu'un utilisateur de niveau administrateur existe. Pour créer un tel utilisateur, exécuter la commande suivante (à exécuter en tant que `root`):

``` bash
flop_admin createsuperuser
```


