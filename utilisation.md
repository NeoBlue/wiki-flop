Il existe deux cas types d'utilisation du logiciel:
1. les usagères et usagers de l'établissement, enseignant⋅e⋅s ou étudiant⋅e.s
2. les gestionnaires, en charge de la gestion des utilisateurs et des ressources disponibles, ou responsables de la fabrication des emplois du temps.


# Les usagères et usagers
Voici les différentes fonctionnalités qui sont offertes à toute personne connectée sur l'application:

## Consulter l'emploi du temps

### Via l'interface
Une scrollbar permet de choisir la semaine, l'emploi du temps en cours est alors affiché:

<img src="images/Tout.png" alt="Un emploi du temps" width="60%"/>

Il est possible de filtrer les cours pour ne faire apparaître que ceux d'un ou plusieurs groupes:

<img src="images/filtre_groupe.gif" alt="filtre par groupes" width="60%"/>

Il est possible de mettre en surbrillance les cours d'un ou plusieurs enseignants, modules, salles:

<img src="images/filtre_prof.gif" alt="filtre par enseignant" width="60%"/>

### Via d'autres interfaces

- iCal
- xflop
- notifications mail

## Exprimer ses contraintes et préférences

### Définir ses préférences types
#### indisponibilités et préférences horaires
#### nombre d'heures par jour
#### préférences de salles
#### notifications

### Définir ses préférences pour une semaine donnée
heures dispos Vs heures à donner


## Modifier l'emploi du temps

## Contacter d'autres utilisatrice⋅eur⋅s.

## Voir et réserver une salle
### Affichage total
Pour accéder au reservations, il faut cliquer sur le bouton 'Reservations'.

Toutes les salles sont affichées, les cours associés à ces salles sont triés dans l'ordre chronologique et ensuite se placent les réservations.

La couleur du nom de la salle change selon le nombre de cours et de réservation dans la semaine (peu occupé vert, normal jaune, trop occupé rouge).

Les reservations sont dans une case de couleurs différente selon leur type de reservation.

Plusieurs filtres peuvent être actifs en même temps.

Les boutons 'plus' renvoie sur un formulaire de création de réservation.

<img src="images/reservT_to_addRes.gif" alt="reservT to addRes" width="60%"/>

Les boutons 'plus' permettant de créer des réservations ne sont visibles que si l'utilisateur est connecté en tant que professeur ou super_utilisateur.

<img src="images/no_user_button_add.png" alt="no user button add" width="60%"/>

Il est possible de changer de salle de 2 manières différente, par la liste déroulante en haut à gauche de l'écran, ou en cliquant sur le nom d'une salle

<img src="images/change_room.gif" alt="cahnge room" width="60%"/>

### Affichage par salle
L'affichage par salle affiche tous les cours et réservations dans l'ordre chronologique en respectant les horaires affiché sur la gauche

<img src="images/reservS.png" alt="reservS" width="60%"/>

La liste déroulante affiche la salle actuelle. 

Les horaires sur la gauche sont calculé en fonction de l'horaire de début minimal et l'horaire de fin maximal des départements, il ne faut donc pas s'inquiéter si l'affichage n'affiche pas vos horaires normaux.

Les reservations sont dans une case de couleurs différente selon leur type de reservation.

Les boutons plus sont en fin de chaque jour.

Une légende des types de réservations indique quelle couleur correspond à quel type de réservation.

### Formulaire de réservation
Le formulaire est séparé en 2 parties, normal et périodicité.

<img src="images/form_periodicity.gif" alt="form periodicity" width="60%"/>

Il y a 3 type de périodicité disponible :

Le 1er, par semaine, la réservation sera reproduite chaque n semaines au jour choisi.

Le 2ème, chaque mois à la même date, la réservation sera reproduite chaque mois à la même date.

Le 3ème, par mois, la réservation sera reproduite tous les Xème Y du mois.

Une fois le formulaire de réservation rempli et la réservation possible, l'utilisateur et renvoyé sur la page de l'affichage total. En cas d'impossibilité de réservation, l'utilisateur est prévenu par un message et dois revenir en arrière pour modifier sa réservation.

<img src="images/form_impossible_possible.gif" alt="form impossible possible" width="60%"/>


# Les gestionnaires
Voici les différentes fonctionnalités ouvertes aux gestionnaires

## Gestion des données : 


### Données de base (Enseignants, salles, groupes d'étudiants, modules, types de cours, horaires, ...)
Les données peuvent être définies via une interface ad hoc baptisée flop!Editor, intégrée à l'outil, et dont vous trouverez la documentation complète par exemple [ici](https://flopedt.iut-blagnac.fr/flopeditor/flopeditor-help/).
Cependant, pour éviter l'usine à clics, l'import initial des données (probablement très nombreuses) se fait via un système de tableurs importés par l'application. Voir la section [import](import#import-des-données-de-base).


### Gestion des contraintes

<img src="images/panel_gestionnaire.png" alt="Panneaux des gestionnaires" width="60%"/>


#### Affichage des contraintes : 

Au centre nous avons les contraintes actives, chaque carte correspond à une contrainte. Sur la carte nous voyons le titre, le nombre de paramètres, le poids,  et une checkbox. Cette checkbox sert à activer ou désactiver la contrainte.
A droite nous avons les contraintes désactivées. Il est possible de plier/déplier les contraintes désactivées pour ne plus les voir affichées.
A gauche la bar "Rechercher" sert à trier les contraintes selon un prof, une salle, une semaine…

#### Gérer les données importé

Les 4 boutons en haut à gauche servent au début de votre session de travail ainsi qu'à la fin.

Fetch contrainte : Actualise les contraintes avec la dernière mise à jour
Nouvelle contrainte : Permet d’ajouter une nouvelle contrainte
Discard changes : Annule les changement appliqués aux contraintes
Commit changes : Enregistre les changements effectués aux contraintes

#### Gérer les contraintes de manière spécifique

Le bandeau du haut agit uniquement sur la contrainte affichée (la dernière sur laquelle nous avons cliqué)
Le titre de la contrainte est à gauche, nous retrouvons ensuite tous les paramètres de la contrainte. Chaque paramètre peut être changé, ajusté.
Le bouton d’activation permet d'activer ou non la contraintes
Le poids permet de définir l’importance de la contrainte/préférence.
Un poids égal à 0 signifie que la contrainte DOIT être respectée.
Si le poids est entre 1 et 8 alors la contrainte devient une préférence. Plus le poids est élevé, plus le solveur essayera de la respecter.
Update : Met à jours les changement que l’on a fait
Duplicate : Crée une nouvelle contrainte avec les même paramètres

#### Gérer les contraintes de manière générique

Le bandeau du bas agit seulement sur les contrainte sélectionnés (celle en bleu)
Pour sélectionner/désélectionner une contrainte il suffit de cliquer dessus.. 
/!\ Attention des contraintes désactivées peuvent aussi être sélectionnées. /!\
Les paramètre communs à toutes les contraintes sélectionnés s’afficheront et vous pourrez les changer
Sur ce bandeau il est possible de changer le poid de plusieurs contraintes en même temps 
/!\ appuyé sur “Maj poid” pour enregistrer le nouveau poid uniquement /!\
Appuyé sur update si vous avez changé des paramètres

- 
## Générer l'emploi du temps 
d'une ou plusieurs semaines, pour tout l'établissement ou une partie de celui-ci

## En cas d'infaisabilité pour une semaine donnée
### Être informé⋅e de l'infaisabilité d'un emploi du temps
### Comprendre les raisons de cette infaisabilité

